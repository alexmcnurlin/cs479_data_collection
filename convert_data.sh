#! /usr/bin/env bash
pushd $1

# Convert vtt to srt
for file in *.vtt; do
	ffmpeg -loglevel -8 -y -i "$file" "${file%.vtt}.srt"
	dos2unix "${file%.vtt}.srt" # Fix line endings
done

# Convert srt to txt transcript
python3 ../srt_to_txt.py *.srt
popd
