fetch:
	bash fetch_data.sh source_data

convert:
	bash convert_data.sh source_data

sort:
	bash sort_data.sh source_data

tag:
	python3 tag_data.py "Josh Martin" source_data/josh_martin/*.json
	python3 tag_data.py "Keith Wieser" source_data/keith_wieser/*.json

aggregate:
	python3 aggregate.py source_data/josh_martin/*.json source_data/keith_wieser/*.json

clean:
	rm -rf source_data/josh_martin source_data/keith_wieser source_data/matthew_young source_data/other source_data/unknown
	rm -f source_data/*.json
	# rm -f source_data/*.txt
	# rm -f source_data/*.srt

nuke: clean
	rm source_data/*.vtt

# %.json: ${FOR}/%.f95 
# 	${FOR_C} ${FOR_D_ARGS} -c ${FOR_MAT_LIB} -o ${FOR_D_MAT_O}
# 	${FOR_C} ${FOR_D_ARGS} ${FOR_D_MAT_O} $< -o $@
