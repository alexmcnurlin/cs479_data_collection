#! /usr/bin/env bash
pushd $1

# Download subtitles
youtube-dl "https://www.youtube.com/watch?v=IjTUaKExHls&list=PLjD5nq-i4AnPN4ifvFiAjkIiWY4ArcNo-" --write-auto-sub --skip-download

# # Download subs for single video (for test)
# youtube-dl "https://www.youtube.com/watch?v=IjTUaKExHls" --write-auto-sub --skip-download

# wordcloud_cli --text *.txt --height 1080 --width 1920 --background white --imagefile output.png
popd
