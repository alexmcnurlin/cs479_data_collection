rm(list=ls())
library("rjson")
df <-fromJSON(file="aggregate.json")
df <- data.frame(count = unlist(df))
df$id <- rownames(df)
rownames(df) <- NULL
df$pastor <- lapply(strsplit(as.character(df$id), "\\."), "[", 1)
df$numwords <- lapply(strsplit(as.character(df$id), "\\."), "[",2)
df$word <- lapply(strsplit(as.character(df$id), "\\."), "[",3)

commonwords <- c("the", "be", "to", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with", "he", "as", "you", "do", "at", "this", "but", "his", "by", "from", "they", "we", "say", "her", "she", "or", "will", "an", "my", "one", "all", "would", "there", "their", "what", "so", "up", "out", "if", "about", "who", "get", "which", "go", "when", "is")

df = df[!(df$word %in% commonwords), ]

plotentries <- 10
op <- par(las=2, mar=c(7, 4, 4, 2))

dfj1 = tail(df[df$pastor == "Josh Martin" & df$numwords == "1", c("count", "word")], n=plotentries)
dfk1 = tail(df[df$pastor == "Keith Wieser" & df$numwords == "1", c("count", "word")], n=plotentries)
png("josh_martin_1_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(7, 4, 4, 2))
barplot(dfj1$count, names.arg=dfj1$word, cex.names=1.0, main="Josh Martin top words", new=TRUE, ylab="Average word use per sermon")
dev.off()
png("keith_wieser_1_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(7, 4, 4, 2))
barplot(dfk1$count, names.arg=dfk1$word, cex.names=1.0, main="Keith Wieser top words", new=TRUE, ylab="Average word use per sermon")
dev.off()

dfj2 = tail(df[df$pastor == "Josh Martin" & df$numwords == "2", c("count", "word")], n=plotentries)
dfk2 = tail(df[df$pastor == "Keith Wieser" & df$numwords == "2", c("count", "word")], n=plotentries)
png("josh_martin_2_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(7, 4, 4, 2))
barplot(dfj2$count, names.arg=dfj2$word, cex.names=1.0, main="Josh Martin top two word phrases", new=TRUE, ylab="Average phrase use per sermon")
dev.off()
png("keith_wieser_2_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(7, 4, 4, 2))
barplot(dfk2$count, names.arg=dfk2$word, cex.names=1.0, main="Keith Wieser top two word phrases", new=TRUE, ylab="Average phrase use per sermon")
dev.off()

dfj3 = tail(df[df$pastor == "Josh Martin" & df$numwords == "3", c("count", "word")], n=plotentries)
dfk3 = tail(df[df$pastor == "Keith Wieser" & df$numwords == "3", c("count", "word")], n=plotentries)

png("josh_martin_3_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(7, 4, 4, 2))
barplot(dfj3$count, names.arg=dfj3$word, cex.names=1.0, main="Josh Martin top three word phrases", new=TRUE, ylab="Average phrase use per sermon")
dev.off()

png("keith_wieser_3_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(7, 4, 4, 2))
barplot(dfk3$count, names.arg=dfk3$word, cex.names=1.0, main="Keith Wieser top three word phrases", new=TRUE, ylab="Average phrase use per sermon")
dev.off()

dfj4 = tail(df[df$pastor == "Josh Martin" & df$numwords == "4", c("count", "word")], n=plotentries)
dfk4 = tail(df[df$pastor == "Keith Wieser" & df$numwords == "4", c("count", "word")], n=plotentries)

png("josh_martin_4_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(8, 4, 4, 2))
barplot(dfj4$count, names.arg=dfj4$word, cex.names=0.9, main="Josh Martin top four word phrases", new=TRUE, ylab="Average phrase use per sermon")
dev.off()

png("keith_wieser_4_word_phrases.png", width=900, height=600)
op <- par(las=2, mar=c(8, 4, 4, 2))
barplot(dfk4$count, names.arg=dfk4$word, cex.names=0.9, main="Keith Wieser top four word phrases", new=TRUE, ylab="Average phrase use per sermon")
dev.off()

