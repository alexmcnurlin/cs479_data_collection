#! /usr/bin/env python3

import json
import re
import sys
from IPython import embed
from collections import OrderedDict

res = [
    re.compile("^[0-9:,]+ --> .*\n$", re.MULTILINE),
    re.compile("^\s+$", re.MULTILINE),
    re.compile("^[0-9]+\n", re.MULTILINE)
]

for arg in sys.argv[1:]:
    print("Opening", arg)
    with open(arg, "r") as f:
        data = f.readlines()

    data = [line for line in data if not any(regex.match(line) for regex in res)]
    newdata = ""
    if not range(len(data)-1):
        print("Opening", arg)
        print("File {} is empty!".format(arg))
        continue
    for i in range(len(data)-1):
        if data[i] != data[i+1]:
            newdata += data[i].replace("\n", " ")
    if data[-1] != newdata[-1]:
        newdata += data[-1].replace("\n", " ")

    with open(arg.replace(".srt", ".txt"), "w") as f:
        f.write(newdata)

    phrase_len = 4
    counts = []
    for i in range(phrase_len+1):
        counts.append({})

    words = newdata.split()
    for i in range(len(words)):
        phrase = words[i:i+phrase_len]
        for n in range(1, phrase_len+1):
            str_phrase = " ".join(phrase[:n])
            if str_phrase not in counts[n]:
                counts[n][str_phrase] = 1
            else:
                counts[n][str_phrase] += 1

    processed_data = {}
    processed_data["metadata"] = {}
    processed_data["data"] = {}
    for i in range(1, phrase_len+1):
        processed_data["data"][i] = OrderedDict()
        for k in sorted(counts[i], key=lambda s: counts[i][s]):
            processed_data["data"][i][k] = counts[i][k]
    with open(arg.replace(".srt", ".json"), "w") as f:
        json.dump(processed_data, f, indent=4)
