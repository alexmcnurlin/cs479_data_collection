#! /usr/bin/env  python3

import json
import sys
import re
import IPython
from collections import OrderedDict

title_re = re.compile(r"^(?:.*/)?(.*)-.{11}.en.json$")

tag_with = sys.argv[1]
for filename in sys.argv[2:]:
    with open(filename, "r") as f:
        data = json.load(f)
    data["metadata"]["pastor"] = tag_with
    title_match = title_re.match(filename)
    if not title_match:
        print("Could not get title for {}".format(filename))
        continue
    data["metadata"]["title"] = title_match.group(1)

    # Re-sort the data
    unsorted_data = data["data"]
    data["data"] = {}
    for dataset in unsorted_data:
        sorted_data = OrderedDict()
        for k in sorted(unsorted_data[dataset], key=lambda s: unsorted_data[dataset][s]):
            sorted_data[k] = unsorted_data[dataset][k]
        data["data"][dataset] = sorted_data

    with open(filename, "w") as f:
        json.dump(data, f, indent=4)
