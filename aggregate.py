import os
import sys
import json
from collections import OrderedDict


def sort_data(mydict):
    """
    Returns a OrderedDict copy of the dictionary with fields sorted
    """
    sorted_data = OrderedDict()
    for k in sorted(mydict, key=lambda s: mydict[s]):
        sorted_data[k] = mydict[k]
    return sorted_data


if len(sys.argv) <= 1:
    sys.stderr.write("Please give filenames to aggregate")


total = {}
total["metadata"] = {"sermon_count": {}}
for filename in sys.argv[1:]:
    with open(filename, "r") as jsonfile:
        data = json.load(jsonfile)

    pastor = data["metadata"]["pastor"]
    if pastor not in total:
        total[pastor] = {}
        total["metadata"]["sermon_count"][pastor] = 0
    total["metadata"]["sermon_count"][pastor] += 1

    # Iterate over sets of data (i.e. word count, 2 word phrase, 3 word phrase, ect)
    for dataset in data["data"]:
        if dataset not in total[pastor]:
            total[pastor][dataset] = {}
        # Merge counts for each word/phrase
        for k in data["data"][dataset]:
            k_l = k.lower()
            if k_l not in total[pastor][dataset]:
                total[pastor][dataset][k_l] = 0
            total[pastor][dataset][k_l] += data["data"][dataset][k]

for pastor in total:
    for dataset in total[pastor]:
        total[pastor][dataset] = sort_data(total[pastor][dataset])

# Normalize word counts (count per sermon)
for pastor in total:
    if pastor != "metadata":
        for dataset in total[pastor]:
            for word in total[pastor][dataset]:
                total[pastor][dataset][word] = total[pastor][dataset][word]/float(total["metadata"]["sermon_count"][pastor])

# Save everything to separate files
for pastor in total:
    if pastor != "metadata":
        for dataset in total[pastor]:
            filename = "logical_data/{}_{}.json".format(pastor, dataset)
            with open(filename, "w") as single_dataset:
                json.dump(total[pastor][dataset], single_dataset, indent=4)


with open("aggregate.json", "w") as agg:
    json.dump(total, agg, indent=4)
