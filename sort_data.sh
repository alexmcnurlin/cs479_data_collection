#! /usr/bin/env bash

pushd $1
mkdir -p josh_martin keith_wieser matthew_young other unknown


grep -il "my name is josh" *.txt | while read file
do
    cp "$file" "${file%.txt}.json" josh_martin/
done


grep -il "my name is keith" *.txt | while read file
do
    cp "$file" "${file%.*}.json" keith_wieser/
done


grep -il "my name is matthew" *.txt | while read file
do
    cp "$file" "${file%.*}.json"  matthew_young/
done


grep -iPl "my name is (chris|drew|jacob|brian|juan|luis)" *.txt | while read file
do
    cp "$file" "${file%.*}.json" other/
done

cp *.txt *.json unknown

popd
